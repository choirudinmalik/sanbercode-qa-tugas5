package webtesting;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTest {
    WebDriver webDriver;

    @BeforeClass
    public void openBrowser() {
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(2000);
        webDriver.quit();
    }

    @AfterMethod
    public void logout() throws InterruptedException {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(1000);
    }

    // Login Case Username and Password True
    @Test(priority = 0)
    public void TC_LOGIN_01() {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
         
        webDriver.findElement(By.id("txt-username")).sendKeys(username);
        webDriver.findElement(By.id("txt-password")).sendKeys(password);
        webDriver.findElement(By.id("btn-login")).click();

        boolean exist = !webDriver.findElements(By.id("btn-book-appointment")).isEmpty();
        Assert.assertTrue(exist); // Button Book appoinment exist
        
        String currentURL = webDriver.getCurrentUrl();
        Assert.assertEquals(currentURL, "https://katalon-demo-cura.herokuapp.com/#appointment");
    }

    // Login Case Username False Password True
    @Test(priority = 1)
    public void TC_LOGIN_02() {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String password = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
         
        webDriver.findElement(By.id("txt-username")).sendKeys("falseuser");
        webDriver.findElement(By.id("txt-password")).sendKeys(password);
        webDriver.findElement(By.id("btn-login")).click();

        boolean isErrorDisplayed = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).isDisplayed();
        Assert.assertTrue(isErrorDisplayed);

        String errorText = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).getText();
        Assert.assertEquals(errorText, "Login failed! Please ensure the username and password are valid.");
    }

    // Login Case Username True Password False
    @Test(priority = 2)
    public void TC_LOGIN_03() {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
         
        webDriver.findElement(By.id("txt-username")).sendKeys(username);
        webDriver.findElement(By.id("txt-password")).sendKeys("wrongpass");
        webDriver.findElement(By.id("btn-login")).click();

        boolean isErrorDisplayed = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).isDisplayed();
        Assert.assertTrue(isErrorDisplayed);

        String errorText = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).getText();
        Assert.assertEquals(errorText, "Login failed! Please ensure the username and password are valid.");
    }

    // Login Case Username False Password False
    @Test(priority = 3)
    public void TC_LOGIN_04() {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        webDriver.findElement(By.id("txt-username")).sendKeys("Username Salah");
        webDriver.findElement(By.id("txt-password")).sendKeys("wrongpass");
        webDriver.findElement(By.id("btn-login")).click();

        boolean isErrorDisplayed = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).isDisplayed();
        Assert.assertTrue(isErrorDisplayed);

        String errorText = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).getText();
        Assert.assertEquals(errorText, "Login failed! Please ensure the username and password are valid.");
    }

    // Data Provider
    @DataProvider(name = "data-login")
    public Object[][] loginData() {
        return new Object[][] { 
            { "John Doe", "ThisIsNotAPassword", true }, 
            { "Dummy user", "ThisIsNotAPassword", false }, 
            { "John Doe", "pass23salh", false },
            { "username22", "passngawur", false }  
        };
    }

    @Test(dataProvider = "data-login")
    public void testMethod(String username, String password, boolean success ) {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        webDriver.findElement(By.id("txt-username")).sendKeys(username);
        webDriver.findElement(By.id("txt-password")).sendKeys(password);
        webDriver.findElement(By.id("btn-login")).click();

        if (success) {
            boolean exist = !webDriver.findElements(By.id("btn-book-appointment")).isEmpty();
            Assert.assertTrue(exist); // Button Book appoinment exist
            
            String currentURL = webDriver.getCurrentUrl();
            Assert.assertEquals(currentURL, "https://katalon-demo-cura.herokuapp.com/#appointment");
        } else {
            boolean isErrorDisplayed = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).isDisplayed();
            Assert.assertTrue(isErrorDisplayed);

            String errorText = webDriver.findElement(By.xpath("//*[@id='login']/div/div/div[1]/p[2]")).getText();
            Assert.assertEquals(errorText, "Login failed! Please ensure the username and password are valid.");
        }
    }
}
